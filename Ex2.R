#1 - ������� ������ ����� ������ ��� ������� �� ������ ������ ���� ����

roundANum <- function(x,g){
  num <- rnorm(1,x,g)
  return(ceiling(num))
}

#2

criticalPath <- function(x){
  A <- roundANum(10,3)
  B <- roundANum(10,3)
  C <- 5
  D <- 10
  E <- roundANum(5,2)
  F <- 4
  
pat1 <- c(A + B + C + F)
pat2 <- c(A + D + E + F)  

  if(pat1 >= pat2)
  cPat <- pat1
  else 
  cPat <- pat2
  
main = c(cPat, pat1, pat2)
return(main)

}

paths <- c(criticalPath())
mainPath <- paths[1]

#3
samples <- 1:10000
paths.vec <- sapply(samples ,criticalPath)
hist(paths.vec,100)


#4
criticalPath4 <- function(x){
  A <- roundANum(10,3)
  B <- roundANum(10,3)
  C <- 5
  D <- 10
  E <- roundANum(5,2)
  F <- 4
  
  pat1 <- c(A + B + C + F)
  pat2 <- c(A + D + E + F)  
  
  if(pat1 >= pat2)
    return(1)
  else 
    return(0)
  
}

samples4 <- 1:10000
paths.vec4 <- sapply(samples4 ,criticalPath4)
ABCF <- paths.vec4[paths.vec4==1] #���� ������ ������� �� ����� ������ ABCF
ADEF <- paths.vec4[paths.vec4==0] #���� ������ ������� �� ����� ������ ADEF

length(ABCF) / 10000 # ���� ������ ��� ABCF ��� ����� ������
